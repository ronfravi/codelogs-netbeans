/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.rexcode.pluginrx.options;

import ec.edu.utpl.datalab.rexcode.base.util.WritterJson;
import ec.edu.utpl.datalab.rexcode.desktop.shell.monitor.CmdCloseProjectWs;
import ec.edu.utpl.datalab.rexcode.desktop.shell.monitor.CmdKillMonitorWs;
import ec.edu.utpl.datalab.rexcode.desktop.shell.monitor.CmdOpenProjectWs;
import ec.edu.utpl.datalab.rexcode.desktop.shell.monitor.SystemSocket;
import ec.edu.utpl.datalab.rexcode.monitor.service.comandos.MonitorCommand;
import static ec.edu.utpl.datalab.rexcode.pluginrx.options.Installer.MONITOR_LB;
import static ec.edu.utpl.datalab.rexcode.pluginrx.options.Installer.RXCODE_HOME;
import ec.edu.utpl.datalab.rexcode.share.ProjectDataAnalysis;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import org.netbeans.api.project.Project;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observer;

public class Rexcode {

    private static final Logger log = LoggerFactory.getLogger(Rexcode.class);

    private static int INTENTOS_CONNECT_SOCKET = 5;
    public static final String CODENAME = "ec.edu.utpl.datalab.rxcode";
    public static final String IDE = "NETBEANS";
    private static Set<String> setClose = new HashSet<>();
    private static Thread thread;
    private static SystemSocket systemSocket = connect();

    public static SystemSocket connect() {
        log.info("Intentando conectar con el servidor monitor");
        int port = NbPreferences.forModule(AjustesPanel.class).getInt(Global.MONITOR_PROC_CMD_PORT, 8484);
        if (INTENTOS_CONNECT_SOCKET > 0 && !isConnected()) {
            --INTENTOS_CONNECT_SOCKET;
            try {
                if (systemSocket == null) {
                    systemSocket = new SystemSocket();
                }
                systemSocket.forceConnection(port);
            } catch (Exception ex) {
                Exceptions.printStackTrace(ex);
            }
        } else {
            log.warn("Se supero el maximo de intentos, no intentara reconectar a un monitor");
        }

        return systemSocket;
    }

    public static void writeCmdOpen(Project project) {
        System.out.println("Enviando comando " + systemSocket.isConnected());
        int port = NbPreferences.forModule(AjustesPanel.class).getInt(Global.MONITOR_PROC_CMD_PORT, 8484);
        try {
            CmdOpenProjectWs openProject = new CmdOpenProjectWs(systemSocket);
            String path = project.getProjectDirectory().getPath();
            path = formatUniversal(path);
            openProject.open(path);
            registerToClose(project);
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public static void writeCmdClose() {
        int port = NbPreferences.forModule(AjustesPanel.class).getInt(Global.MONITOR_PROC_CMD_PORT, 8484);
        for (String project : setClose) {
            try {
                Rexcode.info("Close project " + project);
                CmdCloseProjectWs openProject = new CmdCloseProjectWs(systemSocket);
                openProject.close(project);
            } catch (Exception ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    public static void writeCmdKill() {
        try {
            info("Kill monitor");
            CmdKillMonitorWs ckmw = new CmdKillMonitorWs(systemSocket);
            ckmw.kill();
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public static void writeCmdMetricsCollect(ProjectDataAnalysis analysis) {
        System.out.println("Enviando comando " + systemSocket.isConnected());
        try {
            String body = WritterJson.writte(analysis);
            String cmd = String.format("-%s %s", MonitorCommand.CMD_COLLECT_DATA, body);
            writeCommand(cmd);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public static void writeCommand(String command) {
        try {
            System.out.println("Enviando comandos " + command + " status " + systemSocket.isConnected());
            systemSocket.send(command);
        } catch (Exception e) {
            log.error("Imposible enviar comando");
        }
    }

    private static void registerToClose(Project p) {
        String project = p.getProjectDirectory().getPath();
        setClose.add(project);
    }

    public static boolean isConfig() {
        String keyap = Global.SYNC_APIKEY;
        String keyServer = Global.SYNC_SERVER;
        String keyProtocol = Global.SYNC_PROTOCOL;

        String server = NbPreferences.forModule(AjustesPanel.class).get(keyServer, "");
        String protoco = NbPreferences.forModule(AjustesPanel.class).get(keyProtocol, "");
        String api = NbPreferences.forModule(AjustesPanel.class).get(keyap, "");

        if (api.isEmpty() || server.isEmpty() || protoco.isEmpty()) {
            System.out.println("Sout no se inicio el server [sin configuraciones]");
            return false;
        }
        return true;
    }

    public static String loadPreferencesArg() {
        String keyap = Global.SYNC_APIKEY;
        String keyServer = Global.SYNC_SERVER;
        String keyProtocol = Global.SYNC_PROTOCOL;

        String server = NbPreferences.forModule(AjustesPanel.class).get(keyServer, "");
        String protoco = NbPreferences.forModule(AjustesPanel.class).get(keyProtocol, "");
        String api = NbPreferences.forModule(AjustesPanel.class).get(keyap, "");
        boolean debug = NbPreferences.forModule(AjustesPanel.class).getBoolean(Global.MONITOR_DEBUG, false);

        StringBuilder builder = new StringBuilder();
        builder.append(String.format(" %s=%s", Global.SYNC_APIKEY, api));
        builder.append(String.format(" %s=%s", Global.SYNC_SERVER, server));
        builder.append(String.format(" %s=%s", Global.SYNC_PROTOCOL, protoco));
        builder.append(String.format(" %s=%s", Global.MONITOR_DEBUG, debug));

        return builder.toString();
    }

    /**
     * public static void attemptConnectProcess() { try {
     * System.out.println("Intendo conectar al proceso"); int valuePort =
     * NbPreferences.forModule(AjustesPanel.class).getInt(Global.MONITOR_PROC_CMD_PORT,
     * 8484); if (!systemSocket.isConnected()) {
     * systemSocket.forceConnection(valuePort); System.out.println("Resultado "
     * + systemSocket.isConnected()); }
     *
     * } catch (InterruptedException ex) { Exceptions.printStackTrace(ex); }
    }*
     */
    public static void start() {

        writeInDebugConsole("Iniciando monitor " + systemSocket.isConnected());
        Rexcode.connect();
        writeInDebugConsole("Server monitor existente " + isOfflineProcess());
        if (isOfflineProcess()) {
            if (Rexcode.isConfig()) {
                writeInDebugConsole("Thread lanzado");
                Runnable procRunnable = new Runnable() {
                    @Override
                    public void run() {
                        String proc = RXCODE_HOME + File.separatorChar + MONITOR_LB;
                        System.out.println(proc);

                        try {
                            String line;
                            System.out.println(proc);
                            Process p = Runtime.getRuntime().exec("java -jar " + proc + " " + Rexcode.loadPreferencesArg());
                            log.info("Run monitor : {}", "java -jar " + proc + Rexcode.loadPreferencesArg());
                            writeInDebugConsole("java -jar " + proc + Rexcode.loadPreferencesArg());
                            InputStream inputStream = p.getInputStream();
                            BufferedReader input
                                    = new BufferedReader(new InputStreamReader(inputStream));

                            while ((line = input.readLine()) != null && thread != null) {
                                writeInDebugConsole(line);
                            }
                            input.close();
                        } catch (IOException ex) {
                            Exceptions.printStackTrace(ex);
                        }

                    }

                };

                thread = new Thread(procRunnable);
                thread.setPriority(Thread.MIN_PRIORITY);
                thread.setDaemon(true);
                thread.start();

            } else {
                System.out.println("Thread no iniciando");
            }
        } else {
            systemSocket.getChannel().subscribe(new Observer<String>() {
                InputOutput out = IOProvider.getDefault().getIO("rxconsole", true);

                @Override
                public void onCompleted() {
                    out.getOut().print("Finish channel");
                }

                @Override
                public void onError(Throwable e) {
                    out.getErr().println(e.getMessage());
                }

                @Override
                public void onNext(String t) {
                    out.getOut().println(t);
                }
            });
        }

    }

    public static boolean isOfflineProcess() {
        return !systemSocket.isConnected();
    }

    public static boolean isConnected() {
        try {
            return systemSocket.isConnected();
        } catch (Exception e) {
            return false;
        }
    }

    public static void restart() {
        info("Restart monitor");
        if (thread != null) {
            thread.interrupt();
            start();
        }
    }

    public static void stop() {
        info("send kill command");
        writeCmdClose();
        writeCmdKill();
        if (thread != null) {
            thread.interrupt();
            thread = null;
        }

    }

    public static void info(String info) {
        log.info(info);
    }

    public static void error(String error) {
        log.info(error);
    }

    public static void warn(String warn) {
        log.info(warn);
    }

    public static String formatUniversal(String url) {
        url = url.replaceAll("\\\\", "/");
        if (url.contains("file://")) {
            return url;
        }
        return "file://" + url;
    }

    public static String formatOs(String url) {
        return url.replace("file://", "");
    }

    public static void writeInDebugConsole(String data) {
        boolean debug = NbPreferences.forModule(AjustesPanel.class).getBoolean(Global.MONITOR_DEBUG, false);
        if (debug) {
            InputOutput out = IOProvider.getDefault().getIO("rxconsole", false);
            out.getOut().println(data);
        }

    }

}
