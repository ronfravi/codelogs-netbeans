/* 
 * The MIT License
 *
 * Copyright 2016 rfcardenas.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ec.edu.utpl.datalab.rexcode.pluginrx.options;

import java.io.File;

/**
 * @author rfcardenas
 */
public class Global {
   public static final String APPLICATION_HOME = System.getProperty("user.home") + File.separatorChar + "rxcode";
    public static final String MONITOR_INACTIVE    = "monitor.max.inactive";
    public static final String MONITOR_DEBUG    = "monitor.max.debug";
    public static final String MONITOR_STEEP       = "monitor.cpu.step";
    public static final String MONITOR_SCAN_SOURCE = "monitor.scan.path";
    public static final String MONITOR_SCAN_SUPPORT = "monitor.scan.support.file";
    public static final String MONITOR_PROC_CMD_SOCKET = "monitor.cmd.sck";
    public static final String MONITOR_PROC_CMD_FILESH = "monitor.cmd.file";
    public static final String MONITOR_PROC_CMD_PORT= "monitor.cmd.port";
    public static final String STORE_PATH = "store.path";
    public static final String LANG_SUPPORT = "lang.support.dir";
    public static final String SYNC_SERVER = "sync.server.host";
    public static final String SYNC_FORCE= "sync.server.force";
    public static final String SYNC_PROTOCOL = "sync.server.sync.protocol";
    public static final String SYNC_APIKEY = "sync.server.sync.apikey";
}
